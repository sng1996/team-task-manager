import requests
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class Payload(BaseModel):
    token: str
    data: dict

class RequestPayload(BaseModel):
    userID: str
    data: dict

@app.post('/project', status_code=201)
async def add_project(payload: Payload):
    req = requests.get('http://localhost:8001/user/' + payload.token + '/id')
    userIDModel = req.json()
    requestPayload = RequestPayload(
        userID = userIDModel['id'],
        data = payload.data
    )
    req2 = requests.post('http://localhost:8002/project/', json = requestPayload.dict())
    return req2.json()