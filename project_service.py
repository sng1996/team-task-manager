from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

projects = [
]

class Payload(BaseModel):
    userID: str
    data: dict

class Project(BaseModel):
    name: str
    creatorID: str

@app.post('/project', status_code=201)
async def add_project(payload: Payload):
    project = Project(
        name = payload.data["name"],
        creatorID = payload.userID
    )
    projects.append(project)
    return project