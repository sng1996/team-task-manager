from fastapi import FastAPI

app = FastAPI()

users = {
    '1111': '1',
    '2222': '2',
    '3333': '3'
}

@app.get('/user/{token}/id')
async def get_user_id(token: str):
    return { 'id': users[token] }